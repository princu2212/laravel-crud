<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function demo()
    {
        /* Data show */
        // $a = DB::select('select * from users');
        // dd($a);

        /* Single data insert */
        // $a = DB::table('users')->insert([
        //     'name' => 'xyz',
        //     'email' => 'xyz05@gmail.com',
        //     'password' => 'xyz@123'
        // ]);
        // if ($a) {
        //     dd('Value Inserted');
        // }

        /* Multiple data insert */
        // $a = DB::table('users')->insert([
        //     [
        //         'name' => 'Rohinee',
        //         'email' => 'rohinee14@gmail.com',
        //         'password' => 'rohinee@123'
        //     ],
        //     [
        //         'name' => 'Sweta',
        //         'email' => 'sweta11@gmail.com',
        //         'password' => 'sweta@123'
        //     ],
        // ]);
        // if ($a) {
        //     dd('Value Inserted');
        // }

        /* Update Data */
        // $query = DB::table('users')->where('name', 'prince')->update(['name' => 'manoj']);
        // if ($query) {
        //     dd("Updated Successfully...!!!");
        // }

        /* Delete Data */
        // $a = DB::table('users')->where('id', 6)->delete();
        // if ($a) {
        //     dd('data deleted');
        // }

        // $a = DB::table('users')->where('id', '>', 2)->delete();
        // if ($a) {
        //     dd('data deleted');
        // }
    }

    public function index()
    {
        $users = User::get();
        return view('users.index', compact('users'));
    }


    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->class = $request->class;
        $user->status = $request->status;
        $user->save();

        return redirect('/');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', ['user' => $user]);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->class = $request->class;
        $user->status = $request->status;
        $user->save();

        return redirect('/');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/');
    }
}
