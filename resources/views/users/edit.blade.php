<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD-OPERATION</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <style>
        .card {
            width: 70%;
        }

    </style>
</head>

<body>
    <div class="container">
        <div class="card mt-3 mx-auto shadow rounded-3">
            <h4 class="card-title text-center pt-3">User Information</h4>
            <hr class="w-75 mx-auto">
            <div class="card-body">
                <form class="form-horizontal mx-auto" action="{{ route('update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group mb-3">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Enter name" name="name"
                                value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                                value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="control-label col-sm-2" for="password">Password:</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" placeholder="Enter password"
                                name="password" value="{{ $user->password }}">
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <div class="d-flex">
                            <label class="control-label col-sm-2" for="gender">Gender:</label>
                            <div class="col-sm-10">
                                <div class="d-flex">
                                    <div class="radio px-3">
                                        <input type="radio" name="gender" value="male"
                                            {{ $user->gender == 'male' ? 'checked' : '' }}>Male
                                    </div>
                                    <div class="radio px-3">
                                        <input type="radio" name="gender" value="female"
                                            {{ $user->gender == 'female' ? 'checked' : '' }}>Female
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="gender" value="other"
                                            {{ $user->gender == 'other' ? 'checked' : '' }}>Other
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="control-label col-sm-2" for="gender">Class:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="class">
                                <option value="1" {{ $user->class == 1 ? 'selected' : '' }}>1</option>
                                <option value="2" {{ $user->class == 2 ? 'selected' : '' }}>2</option>
                                <option value="3" {{ $user->class == 3 ? 'selected' : '' }}>3</option>
                                <option value="4" {{ $user->class == 4 ? 'selected' : '' }}>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Status</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="status">
                                <option value="1" {{ $user->status == '1' ? 'selected' : '' }}>Active</option>
                                <option value="2" {{ $user->status == '2' ? 'selected' : '' }}>In Active
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer mt-3 justify-content-center">
                        <button type="submit" class="btn btn-success">Update</button>
                        <a href="/" class="btn btn-warning">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</body>

</html>
